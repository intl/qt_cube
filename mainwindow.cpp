#include "mainwindow.h"
#include "ui_mainwindow.h"


QT_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    {
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint);
    serial = new QSerialPort(this);
    connect(serial,&QSerialPort::readyRead,this,&MainWindow::serial_rx_data);
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
        {
        ui->port_name_box->addItem(info.portName());
        }

    scene = new QGraphicsScene(this);
    scene->addText("Нажмите подключить",QFont("Arial",10));
    ui->graphics_box->setScene(scene);
    }

void MainWindow::connect_btn_click()
    {
     if(serial->isOpen())
        {
        serial->clear();
        serial->close();
        }
    /**
      * Открываем порт 9600:8n1
      */
    serial->setPortName(ui->port_name_box->currentText());
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->setParity(QSerialPort::NoParity);
    if(serial->open(QIODevice::ReadWrite))
        {
        ui->events_box->append("Порт "+ui->port_name_box->currentText()+" открыт");
        }
    else
        {
        ui->events_box->append("<p style='color:red'>Ошибка: Невозможно открыть порт "+ui->port_name_box->currentText());
        }

    foreach (QGraphicsItem* it, scene->items())
        {
        scene->removeItem(it);
        }
    }

void MainWindow::serial_rx_data()
    {
    QByteArray data = serial->readAll();
    ui->terminal_box->insertPlainText(QString::fromLatin1(data.data()));
    foreach (uint8_t d, data)
        {
        rx_data.push_back(d);
        }

    if(rx_data.size()>=5)
        {
        rx_data.push_back(0);
        QString rx_data_str = QString((const char*)rx_data.data());
        QPen p(Qt::black);
        p.setWidth(1);
        QGraphicsTextItem text;

        foreach (QGraphicsItem* it, scene->items())
            {
            scene->removeItem(it);
            }

        if(QString::compare(rx_data_str,"X_0\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0xff,0,0)));
            scene->addText("X=0",QFont("Arial",16))->setDefaultTextColor(QColor(0,0xff,0xff));
            ui->events_box->append("Кубик повернулся (X = 0)");
            }

        if(QString::compare(rx_data_str,"X_1\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0xff,0xff,0)));
            scene->addText("X=1",QFont("Arial",16))->setDefaultTextColor(QColor(0,0x0,0xff));;
            ui->events_box->append("Кубик повернулся (X = 1)");
            }


        if(QString::compare(rx_data_str,"Y_0\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0,0xff,0)));
            scene->addText("Y=0",QFont("Arial",16))->setDefaultTextColor(QColor(0xff,0x0,0xff));;
            ui->events_box->append("Кубик повернулся (Y = 0)");
            }


        if(QString::compare(rx_data_str,"Y_1\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0,0xff,0xff)));
            scene->addText("Y=1",QFont("Arial",16))->setDefaultTextColor(QColor(0xff,0x0,0x0));;
            ui->events_box->append("Кубик повернулся (Y = 1)");
            }


        if(QString::compare(rx_data_str,"Z_0\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0,0,0xff)));
            scene->addText("Z=0",QFont("Arial",16))->setDefaultTextColor(QColor(0xff,0xff,0));;
            ui->events_box->append("Кубик повернулся (Z = 0)");
            }

        if(QString::compare(rx_data_str,"Z_1\r\n")==0)
            {
            scene->addRect(0,0,96,96,p,QBrush(QColor(0xff,0x0,0xff)));
            scene->addText("Z=1",QFont("Arial",16))->setDefaultTextColor(QColor(0,0xff,0x0));;
            ui->events_box->append("Кубик повернулся (Z = 1)");
            }

        rx_data.clear();
        }
    }

void MainWindow::serial_tx_data(const QByteArray d)
    {
    QString tx_str = QString(d.data());
    ui->events_box->append("Отправлено: "+tx_str);
    ui->terminal_box->insertPlainText(tx_str);
    serial->write(d);
    }

MainWindow::~MainWindow()
    {
    serial->close();
    delete ui;
    }


