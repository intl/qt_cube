#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QGraphicsTextItem>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QPainter>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    /**
     * @brief connect_btn_click Обработчик нажатия кнопки "Подключить"
     */
    void connect_btn_click();
    /**
     * @brief serial_rx_data Обработчик получения данных с порта
     */
    void serial_rx_data();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief serial_tx_data Отправляет данные в порт
     * @param d Данные для отправки
     */
    void serial_tx_data(const QByteArray d);
private:
    Ui::MainWindow *ui;
    QSerialPort* serial; /**< Класс последовательного порта */
    QGraphicsScene* scene;
    QGraphicsRectItem* graphics_rect;
    QGraphicsTextItem* graphics_text;
    std::vector<uint8_t> rx_data;

};

#endif // MAINWINDOW_H
